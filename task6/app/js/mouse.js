var STARTSTATE = 1,
		MOVESTATE = 2,
		ENDSTATE = 4;

var MOUSE_EVENTS_MAP = {
	mousedown: STARTSTATE,
	mousemove: MOVESTATE,
	mouseup: ENDSTATE
}

function MouseAction(callback) {

	this.isPressed = false;
	this.elEvents = ['mousedown'];
	this.docEvents = ['mousemove', 'mouseup'];
	this.callback = callback;

}

MouseAction.prototype.handler = function (e) {
	var state = MOUSE_EVENTS_MAP[e.type];
	if (state & STARTSTATE && e.which === 1) {
		this.isPressed = true;
	}

	if (!this.isPressed) {
		return;
	}

	if (state & ENDSTATE) {
		this.isPressed = false;
	}

	e.preventDefault();

	this.callback(state, e);

};
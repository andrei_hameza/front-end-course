var STARTSTATE = 1,
		MOVESTATE = 2,
		ENDSTATE = 4;

var TOUCH_EVENTS_MAP = {
	touchstart: STARTSTATE,
	touchmove: MOVESTATE,
	touchend: ENDSTATE
}

function TouchAction(callback) {

	this.elEvents = ['touchstart', 'touchmove', 'touchend'];
	this.callback = callback;

}

TouchAction.prototype.handler = function (e) {
	var state = TOUCH_EVENTS_MAP[e.type];
	// debugger;
	// if ((state & (STARTSTATE | MOVESTATE)) && (event.targetTouches.length === 1)) {
	// 		var touch = event.targetTouches[0];
	// }

	e.preventDefault();

	this.callback(state, e.targetTouches[0]);

};
'use strict';
function Slider (_container, _options) {
	var position = 0;

	this.container = document.getElementById(_container);
	this.speed = _options.speed || '0.5s';
	this.timingFunc = _options.timingFunc || 'linear';
	this.mq = _options.mq;
	this.getData = null;
	// this.mq = {};
	// mixin(this.mq, options.mq);
	this.btnNext = document.getElementById(_options.next);
	this.btnPrev = document.getElementById(_options.prev);

	Object.defineProperty(this, 'position', {
		get: function () {
			return position;
		},
		set: function (value) {
			position = value > 0 ? 0 : value;
			return position;
		}
	});

	this._initEvents();
	// this._init();
}

Slider.prototype._init = function () {

	this.items = this.container.children;
	this.itemsCount = this.items.length;
	this.itemWidth = 100/this.itemsCount;
	this.position = 0;

}

Slider.prototype._initEvents = function () {
	var that = this,
			hasOwn = Object.hasOwnProperty,
			key;
	this.btnNext.addEventListener('click', function (e) { that._move('next', 1); }, false);
	this.container.addEventListener('swipeleft', function (e) { that._move('next', 1); }, false);
	this.btnPrev.addEventListener('click', function (e) { that._move('prev', 1); }, false);
	this.container.addEventListener('swiperight', function (e) { that._move('prev', 1); }, false);
	for (key in this.mq) {
		if (hasOwn.call(this.mq, key)) {
			window.matchMedia(key).addListener( function (mq) { that._resize(mq); } );
			that._resize(window.matchMedia(key));
		}
	}
}

Slider.prototype._move = function (direction, page) {
	var n = direction === 'next' ? -1 : 1,
			offset = n*this.step*this.itemWidth*page;
	this.position += offset;
	if (Math.abs(this.position + offset) >= 100) {
		this.getData(this.itemsCount + 1);
	}
	this._doTranslate();
}

Slider.prototype._resize = function (_mq) {
	// debugger;
	if (_mq.matches) {
		this.step = this.mq[_mq.media];
		this.container.style.width = 100*this.itemsCount/this.step + '%';
	}
}

Slider.prototype._doTranslate = function () {
	var that = this;
	this.container.style.WebkitTransform = 'translate3d(' + this.position + '%, 0, 0)';
	this.container.style.transform = 'translate3d(' + this.position + '%, 0, 0)';
}
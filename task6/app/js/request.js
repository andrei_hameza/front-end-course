function jsonpRequest(url, onSuccess, onError) {
	this._url = url;
	this.onSuccess = onSuccess;
	this.onError = onError;

	if (!window.CallbackRegistry) {
		this.cbReg = window.CallbackRegistry = {};
	}
	this.cbReg = window.CallbackRegistry;
	this.cbStack = [];
}

jsonpRequest.prototype.makeRequest = function (params) {
	var that = this,
			status = false,
			callbackName,
			queryStr,
			script;

	callbackName = this.cbStack.pop() || 'func' + String(Math.random()).slice(2);
	this.cbReg[callbackName] = function (data) {
		status = true;
		that.cbReg[callbackName] = undefined;
		that.cbStack.push(callbackName);
		script.remove();
		that.onSuccess(data);
	}

	queryStr = this._url + 'CallbackRegistry.' + callbackName + this.decodeRequestParams(params);
	script = document.createElement('script');
	script.src = queryStr;

	function checkCallback() {
		if (status) {
			return;
		}
		that.cbReg[callbackName] = undefined;
		that.cbStack.push(callbackName);
		script.remove();
		that.onError();
	}

	script.onload = script.onerror = checkCallback;

	document.body.appendChild(script);
};

jsonpRequest.prototype.decodeRequestParams = function(params) {
	var key,
			result = '',
			hasOwn = Object.hasOwnProperty;
	for (key in params) {
		if (hasOwn.call(params, key)) {
			if (key === 'q') {
				params[key] = params[key].replace(/ /g, '+');
			}
			result += '&' + key + '=' + params[key];
		}
	}
	return result;
};

// Don't forget to move stack to object and delete tag sript ???
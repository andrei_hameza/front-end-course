var DIRECTION_LEFT = 1,
		DIRECTION_RIGHT = 2;

function Swipe(_el, _options) {
	this.el = _el;
	this.speed = _options.speed || 0.65;
	this.minDist = _options.minDist || 10;
	this.records = {};

	if ('ontouchstart' in window) {
		this.action = new TouchAction(this._recordData.bind(this));
	} else {
		this.action = new MouseAction(this._recordData.bind(this));
	}

	this._init();
}

Swipe.prototype._init = function() {
	this._addEvents();
};

Swipe.prototype._addEvents = function() {
	var that = this;
	if (this.action.elEvents) {
		this.action.elEvents.forEach( function (e) { that.el.addEventListener(e, that.action.handler.bind(that.action), false); } );
	}
	if (this.action.docEvents) {
		this.action.docEvents.forEach( function (e) { document.addEventListener(e, that.action.handler.bind(that.action), false); } );
	}
};

Swipe.prototype._recordData = function (state, e) {
	if (state & STARTSTATE) {
		this.records = {};
		this.isSwipe = false;
	} else if ((!this.isSwipe) && (state & MOVESTATE)) {
	// debugger;
		if (this.records.first) {
			this.records.last = {
				point: {
					x: e.pageX,
					y: e.pageY
				},
				timestamp: Date.now()
			};
			this._checkSwipe();
		}	else {
			this.records.first = {
				point: {
					x: e.pageX,
					y: e.pageY
				},
				timestamp: Date.now()
			};
		}
	} else if ((this.isSwipe) && (state & ENDSTATE)) {
		this._generateSwipeEvent();
	}
}

Swipe.prototype._checkSwipe = function() {
	var firstrec = this.records.first,
			lastrec = this.records.last,
			deltadist = lastrec.point.x - firstrec.point.x,
			deltatime = lastrec.timestamp - firstrec.timestamp;
	if ((Math.abs(deltadist) >= this.minDist) && (Math.abs(deltadist)/deltatime >= this.speed )) {
		this.isSwipe = true;
		this.direction = deltadist < 0 ? DIRECTION_LEFT : DIRECTION_RIGHT;
	}
};

Swipe.prototype._generateSwipeEvent = function() {
	var swipeEvent = document.createEvent('Event'),
			e = this.direction & DIRECTION_LEFT ? 'swipeleft' : 'swiperight';
	swipeEvent.initEvent(e, true, true);
	this.el.dispatchEvent(swipeEvent);
};
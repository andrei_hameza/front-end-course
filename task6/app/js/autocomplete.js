function Autocomplete(source, menu) {
	var that = this;

	this.source = source;
	this.menu = new Menu(menu);
	this.inputValue = '';
	this.checkInputTimer = null;

	this.jsonp = new jsonpRequest('http://suggestqueries.google.com/complete/search?jsonp=', function (data) {
		var result = [],
				arr = data[1];
		// debugger;
		arr.forEach(function (item) {
			result.push(item[0]);
		});
		that.menu.update(result.splice(0, 5));
	}, that.menu.clear.bind(this.menu));

	this.initEvents();

}

Autocomplete.prototype.initEvents = function () {
	var src = this.source;
	src.addEventListener('focus', this.focusHandler.bind(this), false);
	src.addEventListener('blur', this.blurHandler.bind(this), false);
	src.addEventListener('keydown', this.keydownHandler.bind(this), false);
}

Autocomplete.prototype.keydownHandler = function (e) {
	var KEY_UP = 38,
			KEY_DOWN = 40,
			KEY_ENTER = 13,
			KEY_ESC = 27,
			str = this.source.value,
			menu = this.menu,
			code = e.keyCode;

	switch(code) {
		case KEY_UP:
			menu.prev();
			if (~menu.indexActive) {
				clearInterval(this.checkInputTimer);
				this.checkInputTimer = null;
				this.source.value = menu.children[menu.indexActive].innerHTML;
			} else {
				this.source.value = this.inputValue;
			}
			e.preventDefault();
			break;
		case KEY_ENTER:
			// self.setValue( list.get() || input.val() );
			break;
		case KEY_ESC:
			menu.clear();
			break;
		case KEY_DOWN:
			menu.next();
			clearInterval(this.checkInputTimer);
			this.source.value = menu.children[menu.indexActive].innerHTML;
			this.checkInputTimer = null;
			break;
		default:
			if (!this.checkInputTimer) {
				this.focusHandler();
			}
	}

}

Autocomplete.prototype.focusHandler = function(e) {
	var that = this;
	this.checkInputTimer = setInterval(function () {
		var val = that.source.value;
		if (that.inputValue != val) {
			that.inputValue = val;
			if (!val.trim().length) {
				that.menu.clear();
				return;
			}
			that.jsonp.makeRequest({
					'client': 'youtube',
					'ds': 'yt',
					'q': val
			});
		}
	}, 30);

};

Autocomplete.prototype.blurHandler = function(e) {
	clearInterval(this.checkInputTimer);
	this.menu.clear();
};

function Menu(container) {
	this.container = container;
	this.children = container.children;
	this.indexActive = -1;
}

Menu.prototype.initEvents = function() {
	// addEventListener('click', this.clickHandler.bind(this), false);
};

Menu.prototype.clickHandler = function(e) {
	var target = e.target;
};

Menu.prototype.update = function (options) {
	var n = options.length;
	this.clear();
	options.forEach(function (item) {
		var el = document.createElement('li');
		el.innerHTML = item;
		this.container.appendChild(el);
	}, this);
	this.itemsCount = n;
	this.indexActive = -1;
};

Menu.prototype.clear = function() {
	var el = this.container;
	while (el.firstChild) {
		el.removeChild(el.firstChild);
	}
	this.indexActive = -1;
};

Menu.prototype.toggleActive = function(index) {
	if (index < 0) {
		return;
	}
	this.children[index].classList.toggle('active');
};

Menu.prototype.next = function() {
	var index = this.indexActive;
	if (index === this.itemsCount-1) {
		return;
	}
	this.toggleActive(index);
	index++;
	this.toggleActive(index);
	this.indexActive = index;
};

Menu.prototype.prev = function() {
	var index = this.indexActive;
	if (index === -1) {
		return;
	}
	this.toggleActive(index);
	index--;
	this.toggleActive(index);
	this.indexActive = index;
};
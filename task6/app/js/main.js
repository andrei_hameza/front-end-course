var btn = document.getElementsByClassName('btn-submit')[0];
var lstAutocomplete = document.getElementsByClassName('autocomplete')[0];
var srch = document.getElementsByClassName('search')[0];
var lst = document.getElementById('list-videos');
var template = document.getElementById('template').innerHTML,
		el;
// n = 32;
// for (var i = 0; i < n; i++) {
// 	el = document.createElement('li');
// 	el.innerHTML = template;
// 	el.getElementsByTagName('h2')[0].innerHTML = i;
// 	lst.appendChild(el);
// };
// var template = document.getElementById('component').innerHTML;

// debugger;
var sw = new Swipe(lst, {});
var sl = new Slider('list-videos', {
					mq: {
						'(max-width: 1400px) and (min-width: 1200px)' : 4,
						'(max-width: 1199px) and (min-width: 900px)' : 3,
						'(max-width: 899px) and (min-width: 600px)' : 2,
						'(max-width: 600px) and (min-width: 300px)' : 1
					},
					next: 'btn-next',
					prev: 'btn-prev'
				});


var request1 = new jsonpRequest('http://gdata.youtube.com/feeds/api/videos/?callback=',
							cbRequest,
							function (a) {console.log('no');});
var request2 = new jsonpRequest('http://suggestqueries.google.com/complete/search?jsonp=', null, null);

var atcmpl = new Autocomplete(srch, lstAutocomplete);


var lastWord = '';

srch.addEventListener('keydown', keydownHandler, false);

var curRequest = function (q) {
	return function (res) {
		request1.makeRequest({
			'v': '2',
			'alt': 'json',
			'max-results': '15',
			'start-index': res,
			'q': q
		});
	}
};

function keydownHandler(e) {
	var code = e.keyCode,
			str;
	if (code === 13) {
		str = srch.value.trim();
		removeAllChildren(lst);
		request1.makeRequest({
			'v': '2',
			'alt': 'json',
			'max-results': '15',
			'start-index': '1',
			'q': str
		});
		sl.getData = curRequest(str);
		srch.blur();
	}
}

function removeAllChildren(el) {
	while (el.firstChild) {
		el.removeChild(el.firstChild);
	}
}

function cbRequest(data) {
	// debugger;
	var clips = convertYouTubeResponseToClipList(data),
			n = clips.length,
			record,
			el;
	for (var i = 0; i < n; i++) {
		el = document.createElement('li');
		el.innerHTML = template.replace('%author%', clips[i]['author'])
														.replace('%description%', clips[i]['description'])
														.replace('%imagesrc%', clips[i]['thumbnail'])
														.replace('%imagealt%', clips[i]['title'])
														.replace('%title%', clips[i]['title'])
														.replace('%duration%', clips[i]['duration'])
														.replace('%date%', clips[i]['publishDate'])
														.replace('%views%', clips[i]['viewCount']);
		lst.appendChild(el);
	};
	lst.style.width = 100*lst.children.length/4 + '%';
	sl._init();
}

function convertYouTubeResponseToClipList(rawYouTubeData) {
	 var clipList = [];
	 var entries = rawYouTubeData.feed.entry;
	 if (entries) {
			 // debugger;
			 for (var i = 0, l = entries.length; i < l; i++){
					var entry = entries[i];
					var date = new Date(Date.parse(entry.published.$t));
					var shortId = entry.id.$t.match(/video:.*/).toString().split(":")[1];
					var durtime = convertSecsToMinsHours(entry.media$group.yt$duration.seconds);
					clipList.push({
						id: shortId,
						youtubeLink: "http://www.youtube.com/watch?v=" + shortId,
						title: entry.title.$t,
						thumbnail: entry.media$group.media$thumbnail[2].url,
						description: entry.media$group.media$description.$t,
						duration: durtime,
						author: entry.author[0].name.$t,
						publishDate: date.toUTCString().slice(5, 16),
						viewCount: entry.yt$statistics.viewCount ? entry.yt$statistics.viewCount : 'n/a'
					});
			}
	}
	return clipList;
}

function convertSecsToMinsHours(duration) {
	// var hrs = ~~(duration / 3600),
	// 		mins = ~~((duration - hrs*3600) / 60),
	// 		secs = duration % 60,
	// 		res = '';
	// debugger;
	var d = new Date(duration*1000).toUTCString().slice(-12, -4),
			h = d.slice(0, 2),
			m = d.slice(3, 5),
			s = d.slice(6, 8),
			res = '';
	res = (h === '00' ? '' : h + ':') + ((m === '00') && (h === '00') ? '' : m ) + ':' + s;
	return res;
}

// remove script jsonp
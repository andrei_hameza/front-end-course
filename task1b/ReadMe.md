# README #

* #####Owner: Andrei Hameza#####
* #####Email: hameza@gmail.com#####
* #####Bitbucket page: http://andrei_hameza.bitbucket.org#####

This is [taks#1b](https://github.com/rolling-scopes/front-end-course/wiki/Task-%231b) of [Frontend Training Course](https://http://rolling-scopes.github.io/front-end-course/)

###Requriments:###

 * верстка в соответствии с [макетом](http://rolling-scopes.github.io/front-end-course/tasks/task2.png)
 * использование семантической верстки
 * адекватные ховер стили
 * страничка открывается в последней версии хрома

###Implemented:###

 * W3C valid CSS & HTML
 * IE11+, Firefox28+, Chrome21+, Safari6.1+, Opera12.1+ (caniuse.com)
 * Media queries breakpoints: 400px, 550px, 700px, 800px, 1000px, 1200px
 * Print styles
 * Flexbox layout
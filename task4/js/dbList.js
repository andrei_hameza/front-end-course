"use strict";
/**
 * Implementation of doubly linked list data structure.
 * 
 */

/**
 * Creates an instance of ListNode - linked list node helper data type.
 *
 * @constructor
 * @this {ListNode}
 */

function ListNode() {
	this.value = null;	// data field
	this.next = null;		// next node link
	this.prev = null;		// previous node link
}

/**
 * Creates an instance of DbList - doubly linked list data type.
 *
 * @constructor
 * @this {DbList}
 */

function DbList() {
	this._head = new ListNode();	// head of list
	this._tail = new ListNode();	// tail of list
	this._length = 0;							// number of elements on list
	this._head.next = this._tail;
	this._tail.prev = this._head;
}

DbList.prototype = {
	constructor: DbList,
	// Returns the head of list. If list is empty it returns null.
	head: function () {
		return this._head.next.value;
	},
	// Returns the tail of list. If list is empty it returns null.
	tail: function () {
		return this._tail.prev.value;
	},
	// Inserts new elements at the end of list. It returns object.
	append: function () {
		var node;
		for (var i = 0; i < arguments.length; i++) {
			node = new ListNode();
			node.value = arguments[i];
			node.prev = this._tail.prev;
			node.next = this._tail;
			node.prev.next = node;
			this._tail.prev = node;
			this._length++;
		}
		return this;
	},
	// Deletes element by index. It returns object. Error handling (see method DbList._at())
	deleteAt: function (index) {
		var node;
		node = this._at(index);
		if ( node ) {
			node.prev.next = node.next;
			node.next.prev = node.prev;
			this._length--;
		}
		return this;
	},
	// Returns element by index. Error handling (see method DbList._at())
	at: function(index) {
		var node = this._at(index);
		return node ? node.value : null;
	},
	// Inserts element by index. It returns object. Error handling (see method DbList._at())
	insertAt: function ( index, data) {
		var node, newNode;
		node = this._at(index);
		if (node) {
			newNode = new ListNode();
			newNode.value = data;
			newNode.next = node;
			newNode.prev = node.prev;
			node.prev.next = newNode;
			node.prev = newNode;
			this._length++;
		}
		return this;
	},
	// Applies specified function to each item of the list. It returns object. Error handling (check type of @param func)
	each: function (func) {
		var node;
		try {
			this._checkFunction(func);
		} catch(e) {
			console.error("Error: " + e.message);
			return null;
		}
		node = this._head.next;
		while (node.next) {
			func(node.value);
			node = node.next;
		}
		return this;
	},
	// Returns index of the specified item (first entry). Or -1 if it's not found.
	indexOF: function (data) {
		var node = this._head.next,
				i = 0;
		while (node.next) {
			if (node.value === data) {
				return i;
			}
			node = node.next;
			i++;
		}
		return -1;
	},
	// Rearranges the list's items back-to-front. It returns object. 
	reverse: function () {
		var temp, nodeStart, nodeEnd, n;
		n = this._length/2;									// number of iterations
		nodeStart = this._head;							// head
		nodeEnd = this._tail;								// tail
		for (var i = 0; i < n; i++) {				// swap values
			temp = nodeStart.value;
			nodeStart.value = nodeEnd.value;
			nodeEnd.value = temp;
			nodeStart = nodeStart.next;
			nodeEnd = nodeEnd.prev;
		}
		return this;
	},
	// Returns number of elements on list.
	getLength: function () {
		return this._length;
	},
	// Returns object DbList like a string.
	toString: function () {
		var result;
		result = "DbList:";
		this.each(function (value) { result += " " + value; });
		return result;
	},
	// Returns object DbList like an array.
	toArray: function () {
		var arr = [];
		this.each(function (value) { arr.push(value); });
		return arr;
	},
	// Removes all elements from list. It returns object.
	clear: function () {
		this._head.next = this._tail;
		this._tail.prev = this._head;
		this._length = 0;
		return this;
	},
	// Checks is @param n a number or not.
	_checkNumber: function (n) {
		if (isNaN(parseFloat(n)) || !isFinite(n)) {
			throw new Error(n + " is not a number");
		}
	},
	// Checks is @param n out of range or not.
	_checkRange: function (n) {
		if ((n < 0) || (n >= this._length)) {
			throw new Error(n + " index is out of range");
		}
	},
	// Checks is @param n an interger number or not.
	_checkInteger: function (n) {
		if ( +n !== parseInt(n, 10)) {
			throw new Error(n + " is not an integer number");
		}
	},
	// Checks is @param func a function or not.
	_checkFunction: function (func) {
		if (typeof func !== "function") {
			throw new Error(func + ' is not a function');
		}
	},
	// Returns a node of list by index.
	// @param node is a starting point (head, tail or another node)
	// @param direction is a direction of movement (next or prev)
	// @param index is an index of searching node
	_search: function (node, direction, index) {
		var i = 0;
		node = node[direction];
		while ( i !== index ) {
			i++;
			node = node[direction];
		}
		return node;
	},
	// Returns a node of list by index. It is a private helper function for method DbList.at()
	_at: function (index) {
		try {
		// checking index: number > interger > range, finally it can be number or string number
			this._checkNumber(index);
			this._checkInteger(index);
			this._checkRange(index);
		} catch(e) {
			console.error("Error: " + e.message);
			return null;
		}
		// if the index is closer to head it will start from head
		// if the index is closer to tail it will start from tail
		if (index < this._length/2) {
			return this._search(this._head, "next", +index);
		} else {
			return this._search(this._tail, "prev", this._length - (index + 1));
		}
	}
};
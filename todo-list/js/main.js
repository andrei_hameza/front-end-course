// var app = {
// 	todoList: []
// };
// app.add = function(e) {
// 	var btn = event.target,
// 			task = btn.previousSibling.previousSibling.value,
// 			list = btn.nextSibling.nextSibling;
// 	app.todoList.push(task);
// 	list.appendChild(app.createNode(task));
// 	};
// app.createNode = function (str) {
// 	var newli = document.createElement('li');
// 	newli.innerHTML = str;
// 	return newli;
// };
// app.init = function () {
// 	var btn = document.getElementById('add');
// 	btn.addEventListener('click', app.add);
// };

function todoItem(str) {
	this.description = str;
	this.completed = false;
}

todoItem.prototype.toggleCompleted = function () {
	this.completed = !this.completed;
};

function todoList() {
	this.todos = [];
}

todoList.prototype.add = function (data) {
	var newItem =  new todoItem(data);
	this.todos.push(newItem);
	return newItem;
};

todoList.prototype.remove = function (index) {
	this.todos.splice(index, 1);
	return this;
};

todoList.prototype.toggleCompleted = function (index) {
	this.todos[index].toggleCompleted();
};

function todoView (item) {
	this.tag = 'li';
	this.template = '<input type="checkbox" class="check"' + (item.completed ? 'checked':'') + '>\n' +
									'<svg class="svg-images" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" width="100px" height="100px" viewBox="0 0 100 100" enable-background="new 0 0 100 100" xml:space="preserve">\n' +
											'<path fill="none" d="M16.667,62.167c3.109,5.55,7.217,10.591,10.926,15.75 c2.614,3.636,5.149,7.519,8.161,10.853c-0.046-0.051,1.959,2.414,2.692,2.343c0.895-0.088,6.958-8.511,6.014-7.3 c5.997-7.695,11.68-15.463,16.931-23.696c6.393-10.025,12.235-20.373,18.104-30.707C82.004,24.988,84.802,20.601,87,16"' +
											'stroke="#058E72" stroke-width="10px" stroke-linecap="round" stroke-linejoin="round" stroke-dasharray="126px, 126px">\n' +
											'</path>\n' +
									'</svg>\n' +
									'<span>' + item.description + '</span>\n' +
									'<button class="remove"></button>';
}

todoView.prototype.render = function () {
	var node = document.createElement(this.tag);
	node.innerHTML = this.template;
	node.setAttribute('draggable', 'true');
	return node;
};

// debugger;

var app = app || {};


app.Todos = new todoList();

app.AppView = {
	_list: document.getElementById('todo-list'),
	_completed: document.getElementsByClassName('completed'),
	_left: document.getElementById('left'),
	_done: document.getElementById('done'),
	_footer: document.getElementsByClassName('app-footer')[0],
	add: function (item) {
		var key = 13
		var view = new todoView(item);
		this._list.appendChild( view.render() );
	},
	indexOf: function (item) {
		var childrens = this._list.querySelectorAll('li');
		return [].indexOf.call( childrens, item );
	},
	remove: function (item) {
		var index = this.indexOf(item);
		item.remove();
		return index;
	},
	toggle: function (item) {
		var index = this.indexOf(item);
		item.classList.toggle('completed');
		return index;
	},
	update: function() {
		var n = this._list.children.length;
		this._left.innerText = n - this._completed.length;
		this._done.innerText = this._completed.length;
		if (n === 0) {
			this._footer.style.display = 'none';
		} else {
			this._footer.style.display = 'flex';
		}
	}
};

app.Events = {
	_input: document.getElementById('new-task'),
	_btn: document.getElementById('btn-add'),
	add: function (e) {
		var key = 13,
				task;
		// debugger;
		if ((e.keyCode === key) || (e.target === app.Events._btn)) {
			task = app.Events._input.value.trim();
			if (task) {
				app.AppView.add( app.Todos.add(task) );
			}
			app.Events._input.value = '';
			app.AppView.update();
			localStorage.todos = JSON.stringify(app.Todos);
		}
		// e.preventDefault(); // for button
	},
	remove: function (item) {
		app.Todos.remove( app.AppView.remove(item) );
	},
	toggle: function (item) {
		app.Todos.toggleCompleted( app.AppView.toggle(item) );
	},
	delegate: function (e) {
		var target = e.target;
		if (target.classList.contains('remove')) {
			app.Events.remove(target.parentNode);
		}
		if (target.classList.contains('check')) {
			app.Events.toggle(target.parentNode);
		}
		app.AppView.update();
	}
};

app.init = function () {
	this.Events._btn.addEventListener( 'click', app.Events.add, false);
	this.Events._input.addEventListener( 'keydown', app.Events.add, false);
	this.AppView._list.addEventListener( 'click', app.Events.delegate, false);
};

app.init();

// var list = document.getElementById('todo-list');
// var srcEl = null;

// function handleDragStart(e) {
// 	var target = e.target;
// 	target.style.opacity = '0.4';
// 	srcEl = target;
// 	e.dataTransfer.effectAllowed = 'move';
// 	e.dataTransfer.setData('text/html', target.innerHTML);
// 	e.stopPropagation();
// }

// function handleDragEnter(e) {
// 	var target = e.target;
// 	// debugger;
// 	if (!srcEl) return;
// 	e.preventDefault();
// 	if (target.tagName === 'LI') {
// 		target.classList.add('over');
// 	}
// }

// function handleDragLeave(e) {
// 	var target = e.target;
// 	if (!srcEl) return;
// 	if (target.tagName === 'LI') {
// 		target.classList.remove('over');
// 	}
// }

// function handleDragOver(e) {
// 	var target = e.target;
// 	if (!srcEl) return;
// 	if ((target === srcEl) || (target.tagName !== 'LI')) {
// 		return;
// 	}
// 	if (e.preventDefault) {
// 		e.preventDefault();
// 		e.stopPropagation();
// 	}
// 	e.dataTransfer.dropEffect = 'move';
// 	return false;
// }

// function handleDragEnd(e) {
// 	var target = e.target,
// 			curtarget = this;
// 	[].forEach.call(curtarget.children, function (el) { el.classList.remove('over')});
// 	if (target) {
// 		target.style.opacity = '1';
// 	}
// }

// function handleDrop(e) {
// 	var target = e.target,
// 			trsData;
// 	e.stopPropagation();
// 	e.preventDefault();
// 	target = (target.tagName === 'LI') ? target : target.parentNode;
// 	if ((srcEl != target) && (target.tagName === 'LI')) {
// 		trsData = e.dataTransfer.getData('text/html');
// 		if (srcEl.innerHTML !== trsData) return;
// 		srcEl.innerHTML = target.innerHTML;
// 		target.innerHTML = trsData;
// 	}
// 	srcEl = null;
// }

// list.addEventListener('dragstart', handleDragStart, false);
// list.addEventListener('dragenter', handleDragEnter, false);
// list.addEventListener('dragleave', handleDragLeave, false);
// list.addEventListener('dragover', handleDragOver, false);
// list.addEventListener('drop', handleDrop, false);
// list.addEventListener('dragend', handleDragEnd, false);
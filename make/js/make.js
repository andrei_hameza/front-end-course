function sum(x, y) {
	return x + y;
}

function mul(x, y) {
	return x * y;
}

function make(a) {
	var arg = [].slice.call(arguments);
	return function (b) {
		if (typeof b === 'function') {
			return arg.reduce(b);
		}
		return make.apply(null, arg.concat([].slice.call(arguments)));
	}
}

function make(a) {
	var arg = [].slice.call(arguments);
	make = function (b) {
		if (typeof b === 'function') {
			return arg.reduce(b);
		}
		[].slice.call(arguments).forEach( function (param) { arg.push(param); } )
		return make;
	}
	return make;
}

function make(a) {
	var b = arguments[arguments.length-1];
	if (typeof b === 'function') {
		return a.concat([].slice.call(arguments).slice(1, -1)).reduce(b);
	}
	return make.bind(null, [].concat(a).concat([].slice.call(arguments).splice(1)));
}
module.exports = function (grunt) {


	grunt.loadNpmTasks('grunt-contrib-jshint');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-cssmin');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-clean');
	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-usemin');

	grunt.initConfig({
		app: {
			src: './app',
			dest: './dist',
			tmp: './.tmp'
		},
		clean: {
			build: ['<%= app.tmp %>', '<%= app.dest %>'],
			js: ['<%= app.src %>/js/app.min.js']
		},
		copy: {
			html: {
				src: '<%= app.src %>/index.html',
				dest: '<%= app.dest %>/index.html'
			},
			image: {
				src: './bower_components/todomvc-common/bg.png',
				dest: '<%= app.dest %>/css/bg.png'
			}
		},
		useminPrepare: {
			html: '<%= app.src %>/index.html',
				options: {
					dest: '<%= app.dest %>'
				}
		},
		usemin: {
			html: ['<%= app.dest %>/index.html']
		},
		jshint: {
			all: ['<%= app.src %>/js/**/*.js']
		},
		uglify: {
			all: {
				files: {
					'<%= app.src %>/js/app.min.js': [
							'<%= app.src %>/js/models/todo.js',
							'<%= app.src %>/js/collections/todos.js',
							'<%= app.src %>/js/views/todo-view.js',
							'<%= app.src %>/js/views/app-view.js',
							'<%= app.src %>/js/routers/router.js',
							'<%= app.src %>/js/app.js'
						]
				}
			}
		},
		cssmin: {
			combine: {
				files: {
					'<%= app.src %>/css/base.min.css': ['./bower_components/todomvc-common/base.css']
				}
			}
		}
	});

	grunt.registerTask('default', ['clean:js', 'jshint', 'uglify', 'cssmin']);

	grunt.registerTask('build',[
		'clean:build',
		'copy:html',
		'copy:image',
		'useminPrepare',
		'concat:generated',
		'cssmin:generated',
		'uglify:generated',
		'usemin'
	]);
}
'use strict';

var app = angular.module('angularTask', [
  'ngAnimate',
  'ngCookies',
  'ngTouch',
  'ngSanitize',
  'ngResource',
  'ngRoute',
  'ui.bootstrap',
  'appServices',
  'appControllers',
  'appDirectives'
]);

app.config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'app/main/main.html',
        controller: 'MainCtrl'
      })
      .when('/search', {
        templateUrl: 'app/main/main.html',
        controller: 'MainCtrl'
      })
      .when('/search/keyword/:keyword/forks/:forks?/stars/:stars?/size/:?size', {
        templateUrl: 'app/main/main.html',
        controller: 'MainCtrl'
      })
      .when('/repos/:repo/user/:user', {
        templateUrl: 'app/main/repo-details.html',
        controller: 'RepoDetailsCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  })
;

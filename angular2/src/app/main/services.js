'use strict';

var appServices = angular.module('appServices', ['ngResource']);

appServices.factory('githubAPI',
	function($resource) {
		return $resource('', {},
			{
				searchRepos: {
					method: 'GET',
					url: 'https://api.github.com/search/repositories?q=:keyword&forks>=:forks&stars>=:stars&size<=:size&page=:page&per_page=:perPage',
				},
				getRepo: {
					method: 'GET',
					url: 'https://api.github.com/repos/:user/:repo'
				},
				getContributors: {
					method: 'GET',
					isArray: true,
					url: 'https://api.github.com/repos/:user/:repo/contributors'
				}
			}
		);
	}
);

appServices.factory('dataStorage', function(){
	var data = [];
	var query = {};
	return {
		addData: function (arr) {
			Array.prototype.push.apply(data, arr);
		},
		getData: function () {
			return data;
		},
		addQuery: function (item) {
			query = item;
		},
		getQuery: function () {
			return query;
		}
	};
});
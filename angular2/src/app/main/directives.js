'use strict';

var appDirectives = angular.module('appDirectives', []);

appDirectives.directive('myscroll', function ($window) {
	return {
		restrict: 'A',
		link: function (scope) {
			var win = angular.element($window);
			var scrollHandler = function () {
				var doc = angular.element($window.document),
						scrollTop = win.scrollTop(),
						winHeight = win.height(),
						docHeight = doc.height();
				if ((scope.active) && (docHeight - scrollTop <= winHeight)) {
					scope.$emit('myscroll');
				}
			};
			win.bind('scroll', scrollHandler);
			scope.$on('$destroy', function() {
				win.unbind('scroll', scrollHandler);
			});
		}
	};
});

appDirectives.directive('mygallery', function() {
	return {
		restrict: 'E',
		scope: {
			items: '='
		},
		templateUrl: 'app/main/my-gallery.html'
	};
});

appDirectives.directive('myform', function() {
	return {
		restrict: 'E',
		scope: {
			request: '='
		},
		controller: function ($scope, $rootScope) {
			$scope.fire = function () {
				$rootScope.$broadcast('mysubmit');
			};
		},
		templateUrl: 'app/main/my-form.html'
	};
});
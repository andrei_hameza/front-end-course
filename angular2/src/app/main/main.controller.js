'use strict';

var appControllers = angular.module('appControllers', ['appServices']);

appControllers.controller('MainCtrl', function ($scope, $location, githubAPI, dataStorage) {

		$scope.request = dataStorage.getQuery();
		$scope.request.perPage = 12;
		$scope.request.page = 1;
		$scope.items = dataStorage.getData();
		$scope.active = $scope.items.length ? true : false;

		$scope.makeRequest = function (page) {
			var response;
			$scope.active = false;
			$scope.request.page = page || ($scope.request.page + 1);
			response = githubAPI.searchRepos($scope.request);
			response.$promise.then(function () {
				[].push.apply($scope.items, response.items);
				$scope.active = true;
			});
		};

		$scope.$on('myscroll', function () {
			$scope.makeRequest();
		});

		$scope.$on('mysubmit', function () {
			$scope.items.length = 0;
			$scope.makeRequest(1);
			// $location.path('/search/keyword');
		});
});

appControllers.controller('RepoDetailsCtrl', function ($scope, $routeParams, githubAPI) {
		$scope.item = githubAPI.getRepo({
			repo: $routeParams.repo,
			user: $routeParams.user
		});
		$scope.contributors = githubAPI.getContributors({
			repo: $routeParams.repo,
			user: $routeParams.user
		});
});


// Update service restapi -> github api with multiple methods
// Update scroll handler
// form directive
// scroll gallery directive
// events
// history based on routes

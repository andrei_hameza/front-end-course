describe('Game', function() {

	var att = 3,
			min = 1,
			max = 10,
			gm;

	beforeEach(function(){
		gm = new Game(att, min, max);
		 //  gm.getHint = spy;
			// gm.getHint.should.have.been.called();

		// gm.init();
	});

	describe('#testing object gm', function() {

		it('should be instance of Game', function(){
			gm.should.be.an.instanceof(Game);
		});

		var keys = ['_maxAttemptsCount', '_attemptsCount', '_maxNum', '_minNum', '_state', '_answer', '_hint'];

		it('should have keys ' + keys, function(){
			gm.should.have.keys(keys);
		});

		it('should have property ' + keys[0] + ' that is number and equal ' + att, function(){
			gm.should.have.property(keys[0])
				.that.is.a('number')
					.is.equal(att);
		});

		it('should have property ' + keys[3] + ' that is number and equal ' + min, function(){
			gm.should.have.property(keys[3])
				.that.is.a('number')
					.is.equal(min);
		});

		it('should have property ' + keys[2] + ' that is number and equal ' + max, function(){
			gm.should.have.property(keys[2])
				.that.is.a('number')
					.is.equal(max);
		});

	});

	describe('#init()', function() {

		beforeEach(function () {
			gm._generateNumber = spy;
			gm.init();
		});

		it('should call _generateNumber once to set _answer property', function(){
			gm._generateNumber.should.have.been.called.once;
		});

		it('should set RUN_GAME state', function(){
			gm._state.should.be.equal(RUN_GAME);
		});

		it('should set _attemptsCount property equal to _maxAttemptsCount', function(){
			gm._attemptsCount.should.be.equal(gm._maxAttemptsCount);
		});

	});

	describe('#_generateNumber() ', function() {
		var arr,
				n = 10000,

				m = (max + min)/2,
				d = Math.pow((Math.pow(max - min + 1, 2) - 1)/12, 0.5),
				freqP = 1 / (max-min+1);

		beforeEach(function () {
			var rand,
					length = max - min;
			arr = [];
			for (var i = 0; i < n; i++) {
				arr.push(gm._generateNumber());
			};
		});

		it('should generate array of ' + n + ' numbers that should have math expectation almost equal ' + m, function() {
			arr.should.have.mean.almost.equal(m, 1);
		});

		it('should generate array of ' + n + ' numbers that should have deviation almost equal ' + d, function() {
			arr.should.have.deviation.almost.equal(d, 1);
		});

		it('should generate array of ' + n + ' numbers that should have uniform distribution with p~' + freqP, function() {
			var freqArr = [];
			for (var i = 0; i < max - min + 1; i++) {
				freqArr.push(0);
			};
			for (i = 0; i < n; i++) {
				freqArr[arr[i] - min]++;
			};
			// debugger;
			freqArr.forEach(function (item, index) {freqArr[index] /= n});
			freqArr.should.have.mean.almost.equal(freqP, 1);
		});


	});

	describe('#_checkNumber()', function() {
		var a = {},
				b = true,
				c = "5",
				d = 3;

		it('should throw error: ' + a + ' is not a number if param is an object', function(){
			gm._checkNumber.bind(gm, a).should.throw(a + ' is not a number');
		});

		it('should throw error: ' + b + ' is not a number if param is a boolean true', function () {
			gm._checkNumber.bind(gm, b).should.throw(b + ' is not a number');
		});

		it('should not throw error if param is a string ' + c, function() {
			gm._checkNumber.bind(gm, c).should.not.throw(c + ' is not a number');
		});

		it('should not throw error if param is a number ' + d, function() {
			gm._checkNumber.bind(gm, d).should.not.throw(d + ' is not a number');
		});

	});

	describe('#_checkRange()', function() {
		var range = '[' + min + ';' + max + ']',
				a = min - 1,
				b = max + 1,
				c = max - 1;

		it('should throw error: ' + a + ' is out of range ' + range, function(){
			gm._checkRange.bind(gm, a).should.throw(a + ' is out of range');
		});

		it('should throw error: ' + b + ' is out of range ' + range, function(){
			gm._checkRange.bind(gm, b).should.throw(b + ' is out of range');
		});

		it('should not throw error: ' + c + ' belongs to range ' + range, function(){
			gm._checkRange.bind(gm, c).should.not.throw(c + ' is out of range');
		});

	});

	describe('#_checkInteger()', function() {
		var a = 2.5,
				c = 5;

		it('should throw error: ' + a + ' is not an integer number', function(){
			gm._checkInteger.bind(gm, a).should.throw(a + ' is not an integer number');
		});

		it('should not throw error: ' + c + ' is an integer number', function(){
			gm._checkInteger.bind(gm, c).should.not.throw(c + ' is not an integer number');
		});

	});

});
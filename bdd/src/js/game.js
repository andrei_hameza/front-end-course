"use strict";
var	START_GAME = 1;
var	RUN_GAME = 2;
var	WIN_GAME = 4;
var LOST_GAME = 8;

function Game($maxAttemptsCount, $minNum, $maxNum) {

	this._maxAttemptsCount = $maxAttemptsCount;
	this._attemptsCount = 0;
	this._maxNum = $maxNum;
	this._minNum = $minNum;
	this._state = START_GAME;
	this._answer = null;
	this._hint = "";

}

Game.prototype.init = function () {
	this._state = RUN_GAME;
	this._answer = this._generateNumber();
	this._attemptsCount = this._maxAttemptsCount;
	this._hint = "";
};

Game.prototype.makeGuess = function (n) {
// debugger;
	if (this._state & RUN_GAME) {
		this._checkNumber(n);
		this._checkInteger(n);
		this._checkRange(n);
		if (+n === this._answer) {
			this._state = WIN_GAME;
			this._hint = "";
		} else {
			this._hint = +n > this._answer ? 'less' : 'greater';
		}
		this._attemptsCount--;
		if (!this._attemptsCount) {
			this._state = LOST_GAME;
			this._hint = "";
		}
	}
};

Game.prototype.getState = function () {
	return this._state;
};

Game.prototype.getHint = function() {
	return (this._state & RUN_GAME) ? this._hint : "";
};

Game.prototype._generateNumber = function() {
	return Math.floor(Math.random() * (this._maxNum - this._minNum + 1)) + this._minNum;
};

Game.prototype._checkNumber = function (n) {
	if (isNaN(parseFloat(n)) || !isFinite(n)) {
		throw new Error(n + " is not a number");
	}
};

Game.prototype._checkRange = function (n) {
	if ((n < this._minNum) || (n > this._maxNum)) {
		throw new Error(n + " is out of range");
	}
};

Game.prototype._checkInteger = function (n) {
	if ( +n !== parseInt(n, 10)) {
		throw new Error(n + " is not an integer number");
	}
};
var nmin = 1;
var nmax = 10;
var att = 3;
var play = new Game(att, nmin, nmax);
var elChild, elParent;
var btn = document.getElementsByTagName('button')[0];
elParent = document.getElementsByClassName('game')[0];

btn.addEventListener('click', function () {
	play.init();
	elParent.innerHTML = "";
	for (var i = nmin; i <= nmax; i++) {
		elChild = document.createElement('li');
		elChild.innerHTML = i;
		elParent.appendChild(elChild);
	};
	elParent.addEventListener('click', clickHandler, false);

}, false);


function clickHandler(e) {
	var target = e.target,
			n, state;
// debugger;
	state = play._state;
	if ((state & RUN_GAME) && (target.matches('.game > li')) && (!target.classList.contains('optioned'))) {
		n = target.innerHTML;
		play.makeGuess(n);
		state = play._state;
		switch (state) {
			case RUN_GAME:
				target.classList.add('optioned');
				target.innerHTML = play.getHint();
				break;
			case WIN_GAME:
				target.classList.add('win');
				break;
			case LOST_GAME:
				target.classList.add('optioned');
				break;
		}
	}
}
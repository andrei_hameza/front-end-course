# README #

* #####Owner: Andrei Hameza#####
* #####Email: hameza@gmail.com#####
* #####Bitbucket page: http://andrei_hameza.bitbucket.org#####

This is [taks#1a](https://github.com/rolling-scopes/front-end-course/wiki/Task-%231a) of [Frontend Training Course](https://http://rolling-scopes.github.io/front-end-course/)

###Requriments:###

Implement the ["Holy Grail"](http://en.wikipedia.org/wiki/Holy_Grail_(web_design)) three-column web page layout. Your html page should contain header, content section (three-column), footer [see image below](http://rolling-scopes.github.io/front-end-course/tasks/task1.jpg)
(function () {

	window.MyJQuery = window.$ = function (selector) {
		return new MyJQuery(selector);
	}

	window.$.cache = storage = {
		elements: [],
		data: []
	};

	function MyJQuery(selector) {
		this.selector = selector;
		this.collection = document.querySelectorAll(this.selector);
		if (!this.collection.length) {
			return;
		}
	}

	MyJQuery.prototype.on = function(events, selector, handler) {
		var arrEvents = events.split(' '),
				func = handler,
				that = this;
		if (typeof selector === 'string') {
			func = function (e) {
				var target = e.target;
				if (target.matches(that.selector + ' ' + selector)) {
					handler(e);
				}
			}
		} else {
			func = selector;
		}
		[].forEach.call(this.collection, function (item) {
			arrEvents.forEach(function (str) {
				item.addEventListener(str, func, false);
			});
		});
		return this;
	};

	MyJQuery.prototype.one = function(events, selector, handler) {
		var arrEvents = events.split(' '),
				func = handler,
				that = this;
		if (typeof selector === 'string') {
			func = function (e) {
				var target = e.target;
				if (target.matches(that.selector + ' ' + selector)) {
					handler(e);
					this.removeEventListener(e.type, func, false);
				}
			}
		} else {
			func = function (e) {
				selector(e);
				this.removeEventListener(e.type, func, false);
			}
		}
		[].forEach.call(this.collection, function (item) {
			arrEvents.forEach(function (str) {
				item.addEventListener(str, func, false);
			});
		});
		return this;
	};

	MyJQuery.prototype.data = function(name, value) {
		var result = this,
				n = arguments.length;
		if (name === undefined) {
			return this._getData();
		}
		if (typeof name === 'object') {
			return this._setData(name);
		}
		return n > 1 ? this._setData( getObject(name, value) ) : this._getData(name);
	};

	MyJQuery.prototype._getData = function(name) {
		var el = this.collection[0],
				cacheIndex = storage.elements.indexOf(el);
		if (!~cacheIndex) {
			return;
		}
		return arguments.length ? storage.data[cacheIndex][name] : storage.data[cacheIndex];
	}

	MyJQuery.prototype._setData = function (obj) {
		[].forEach.call(this.collection, function (item) {
			var cacheIndex = storage.elements.indexOf(item),
					elData;
			if (~cacheIndex) {
				elData = storage.data[cacheIndex];
				storage.data[cacheIndex] = mix(elData, obj);
			} else {
				storage.elements.push(item);
				storage.data.push(obj);
			}
		});
		return this;
	}

	MyJQuery.prototype.css = function (property, value) {
		var key,
				result = this,
				n = arguments.length;
		if (n === 1) {
			if (typeof property === 'string') {
				result = this._getCssValue(property);
			}
			if (property.splice) {
				result = {};
				property.forEach(function (item) {
					result[item] = this._getCssValue(item)
				}, this);
			}
			if (typeof property === 'object') {
				for(key in property) {
					if (property.hasOwnProperty(key)) {
						this._setCssValue(key, property[key]);
					}
				}
			}
		}
		if (n === 2) {
			this._setCssValue(property, value);
		}
		return result;
	}

	MyJQuery.prototype._getCssValue = function (property) {
		var el = this.collection[0],
				style = window.getComputedStyle(el, null);
		return style.getPropertyValue(property);
	}

	MyJQuery.prototype._setCssValue = function (property, value) {
		[].forEach.call(this.collection, function (item) {
			item.style[property] = value;
		});
	}

	MyJQuery.prototype.children = function (selector) {
		var str = this.selector + ' > ' + (selector || '*');
		return new MyJQuery(str);
	};

	MyJQuery.prototype.append = function (content) {
		var type = typeof content;

		if (type === 'string') {
			this._appendHTML(content);
		}
		if (type === 'object' && content.toString().slice(-8,-1) === 'Element') {
			this._appendElement(content);
		}
		return this;
	}

	MyJQuery.prototype._appendHTML = function(content) {
		[].forEach.call(this.collection, function (item) {
			var newhtml = item.innerHTML + content;
			item.innerHTML = newhtml;
		});
	};

	MyJQuery.prototype._appendElement = function(content) {
		// debugger;
		[].forEach.call(this.collection, function (item) {
			item.appendChild(content.cloneNode());
		});
	};

	MyJQuery.prototype.attr = function (attrName, value) {
		var result = this,
				n = arguments.length;
		if (n === 1) {
			result = this._getAttrValue(attrName);
		}
		if (n === 2) {
			this._setAttrValue(attrName, value);
		}
		return result;
	};

	MyJQuery.prototype._getAttrValue = function (attrName) {
		return this.collection[0].getAttribute(attrName);
	}

	MyJQuery.prototype._setAttrValue = function (attrName, value) {
		[].forEach.call(this.collection, function (item) {
			item.setAttribute(attrName, value);
		});
	}

	MyJQuery.prototype.addClass = function (classNames) {
		this._changeClass('add', classNames);
		return this;
	};

	MyJQuery.prototype.removeClass = function (classNames) {
		this._changeClass('remove', classNames);
		return this;
	};

	MyJQuery.prototype._changeClass = function (operation, classNames) {
		var arrClassNames = classNames.split(' ');
		[].forEach.call(this.collection, function (item) {
			arrClassNames.forEach(function (str) {
				item.classList[operation](str);
			});
		});
	}

	function getObject(name, value) {
		var obj = {};
		obj[name] = value;
		return obj;
	}

	function mix() {
		var key, i, arg,
				obj = {},
				n = arguments.length;
		for (i = 0; i < n; i++) {
			arg = arguments[i];
			for (key in arg) {
				if (arg.hasOwnProperty(key)) {
					obj[key] = arg[key];
				}
			}
		}
		return obj;
	}

})();
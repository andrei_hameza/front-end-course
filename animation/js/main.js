var toasts = [{message: 'Team: England', color:'#FF170F'},
						 {message: 'Team: Brazil', color:'#FFBB0E'},
						 {message: 'Team: Italy', color:'#007ED2'},
						 {message: 'Team: Spain', color:'#8E2800'}, ];
var btn = document.getElementsByClassName('button')[0];
var list = document.getElementsByClassName('toasts')[0];

function handleAnimationEnd (e) {
	e.target.remove();
}

function handleClick(e) {
	var newLi = document.createElement('li'),
			i = Math.floor(4*Math.random());
	newLi.innerHTML = toasts[i].message;
	newLi.style.backgroundColor = toasts[i].color;
	list.appendChild(newLi);
}

btn.addEventListener('click', handleClick, false);
list.addEventListener('webkitAnimationEnd', handleAnimationEnd, false);
list.classList.toggle('slide');

// setInterval(handleClick, 2000);
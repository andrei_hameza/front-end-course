debugger;
var socket  = new WebSocket('ws://wsc.jit.su'),
		auth_token,
		bits,
		arr;
socket.binaryType ='arraybuffer';

function sendText(msg) {
	socket.send(JSON.stringify(msg));
}


socket.onopen = function (event) {
	sendText({
		msg: 'challenge_accepted',
		name: 'Andrei'
	});
}

socket.onmessage = function (event) {
	var msg,
			res;
	// debugger;
	if ( typeof event.data === 'string') {
		msg = JSON.parse(event.data);
		switch(msg.msg) {
			case 'auth':
				auth_token = msg.auth_token;
				sendText({
					msg: 'task_one',
					auth_token: auth_token
				});
				break;
			case 'compute':
				switch(msg.operator){
					case '+':
						res = msg.operands[0] + msg.operands[1];
						break;
					case '-':
						res = msg.operands[0] - msg.operands[1];
						break;
					case '*':
						res = msg.operands[0] * msg.operands[1];
						break;
				}
				sendText({
					msg: 'task_one_result',
					result: res,
					auth_token: auth_token
				});
				break;
			case 'win':
				sendText({
					msg: 'next',
					auth_token: auth_token
				});
				break;
			case 'binary_sum':
				bits = msg.bits;
				break;
		}
	} else {
		console.log(event);
		debugger;
		if (bits === 8) {
			arr = new Uint8Array(event.data);
		} else {
			arr = new Uint16Array(event.data);
		}
		sendText({
			msg: 'task_two_result',
			result: [].reduce.call(arr, function (a, b) { return a+b; }),
			auth_token: auth_token
		});
	}
}
function Iterator(arr, width, cycle, func) {
	this.arrLength = arr.length;
	this.width = width <= this.arrLength ? width : this.arrLength;
	var startIndex = 0;
	var endIndex = width - 1;
	this.isCyclic = cycle;
	this.data = arr;
	Object.defineProperties(this, {
		startIndex: {
			get: function () {
				return startIndex;
			},
			set: function (value) {
				var n = this.arrLength;
				if (!this.isCyclic && (value + startIndex < 0)) {
					return 0;
				}
				startIndex = (value + n) % n;
				return startIndex;
			}
		},
		endIndex: {
			get: function () {
				return endIndex;
			},
			set: function (value) {
				var n = this.arrLength;
				endIndex = (value + n) % n;
				return endIndex;
			}
		}
	});
	Object.observe(this.data, this._observer.bind(this), ['splice']);
}

Iterator.prototype._observer = function (splice) {
	var addedCount = splice[0].addedCount,
			removedCount = splice[0].removed.length,
			index = splice[0].index,
			delta = this.startIndex - index;
	// debugger;
	this.arrLength += addedCount - removedCount;
	if (removedCount) {
		if (index < this.startIndex) {
			this.startIndex -= removedCount <= delta ? removedCount : delta;
			this.endIndex = this.startIndex + this.width - 1;
		} else if (this.startIndex > this.endIndex) {
			// this.startIndex -= removedCount;
			this.endIndex = this.startIndex + this.width - 1;
		} else {
			// this.endIndex -= removedCount;
		}
	}
	if (addedCount) {
		if (index <= this.startIndex) {
			this.startIndex += addedCount;
		}
		this.endIndex = this.startIndex + this.width - 1;
	}
}

Iterator.prototype._getWindow = function (start, end) {
		return start < end ?
						this.data.slice(this.startIndex, this.endIndex + 1):
						this.data.slice(this.startIndex).concat(this.data.slice(0, this.endIndex + 1));
}

Iterator.prototype.hasNext = function () {
	return this.isCyclic || (this.endIndex < this.arrLength - 1);
}

Iterator.prototype.hasPrevious = function () {
	return this.isCyclic || (this.startIndex > 0);
}

Iterator.prototype.next = function () {
	if (this.hasNext()) {
		this.startIndex++;
		this.endIndex++;
	}
	return this._getWindow(this.startIndex, this.endIndex + 1);
}

Iterator.prototype.previous = function () {
	if (this.hasPrevious()) {
		this.startIndex--;
		this.endIndex--;
	}
	return this._getWindow(this.startIndex, this.endIndex + 1);
}

Iterator.prototype.current = function () {
	return this._getWindow(this.startIndex, this.endIndex + 1);
}

debugger;

var arr = [0, 1, 2, 3, 4, 5, 6, 7];
var it = new Iterator(arr, 3, false);

// var notif = arr.getNotifier()



// console.log(it.next());
// console.log(it.previous());
// console.log(it.next());
// console.log(it.next());
// console.log(it.next());
// console.log(it.next());
// console.log(it.next());
// console.log(it.next());
// console.log(it.next());
// arr.splice(0, 0, 11);

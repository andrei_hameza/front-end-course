'use strict';

var appServices = angular.module('appServices', ['ngResource']);

appServices.factory('Repository', function($resource) {
	return $resource('https://api.github.com/search/repositories?q=:keyword&+forks:>=:forks+stars:>=:stars+size');
});

appServices.factory('Detail', function($resource) {
	return $resource('https://api.github.com/repos/:user/:repo');
});

appServices.factory('historyRepos', function(){
	return {items: []};
});
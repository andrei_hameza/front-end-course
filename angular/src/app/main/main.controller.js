'use strict';

var appControllers = angular.module('angularTaskControllers', ['appServices']);

appControllers.controller('MainCtrl', function ($scope, Repository, historyRepos) {
		$scope.repos = historyRepos || {};
		$scope.makeRequest = function () {
			$scope.repos = Repository.get($scope.request);
			$scope.repos.$promise.then(function () {
				historyRepos.items = $scope.repos.items;
			});
		};
});

appControllers.controller('RepoDetailsCtrl', function ($scope, $routeParams, Detail) {
		$scope.item = Detail.get({
			repo: $routeParams.repo,
			user: $routeParams.user
		});
});

'use strict';

var app = angular.module('angularTask', [
  'ngAnimate',
  'ngCookies',
  'ngTouch',
  'ngSanitize',
  'ngResource',
  'ngRoute',
  'ui.bootstrap',
  'appServices',
  'angularTaskControllers'
]);

app.config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'app/main/main.html',
        controller: 'MainCtrl'
      })
      .when('/repos/:repo/user/:user', {
        templateUrl: 'app/main/repo-details.html',
        controller: 'RepoDetailsCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  })
;
